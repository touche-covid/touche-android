package ai.realvalue.covid;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;

import com.camerakit.CameraKit;
import com.camerakit.CameraKitView;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.util.Timer;
import java.util.TimerTask;

import kotlin.jvm.Synchronized;

public class MainActivity extends AppCompatActivity {

    private CameraKitView cameraKitView;
    private BarcodeDetector detector;
    private FaceDetector faceDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraKitView = findViewById(R.id.camera);


        // Create a QRCode reader
        detector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE).build();

        // Create the face detector
        faceDetector = new FaceDetector(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.takePicture:
                takePicture();
                return true;
            case R.id.flipCameras:
                if (cameraKitView.getFacing() == CameraKit.FACING_FRONT) {
                    cameraKitView.setFacing(CameraKit.FACING_BACK);
                } else {
                    cameraKitView.setFacing(CameraKit.FACING_FRONT);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        cameraKitView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraKitView.onResume();
    }

    @Override
    protected void onPause() {
        cameraKitView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        cameraKitView.onStop();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cameraKitView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    static class AsyncPictureProcessing extends  AsyncTask<byte[],Void,String> {
        BarcodeDetector detector;
        FaceDetector faceDetector;
        AsyncPictureProcessing(BarcodeDetector detector, FaceDetector faceDetector) {
            this.detector = detector;
            this.faceDetector = faceDetector;
        }

        @Override
        protected String doInBackground(byte[]... bytesArray) {
            Bitmap image = BitmapFactory.decodeByteArray(bytesArray[0], 0, bytesArray[0].length);

            faceDetector.process(image);

            SparseArray<Barcode> barcodes = detector.detect(new Frame.Builder().setBitmap(image).build());
            final String prefix = "rv/";
            for (int i = 0; i < barcodes.size(); i++) {
                Barcode code = barcodes.valueAt(0);
                if (code != null && code.rawValue != null && code.rawValue.startsWith(prefix)) {
                    String deviceID = code.rawValue.substring(prefix.length());
                    return "Binding to device = " + deviceID;
                }
            }
            return "No matching barcode detected";
        }

        @Override
        protected void onPostExecute(String message) {
            super.onPostExecute(message);
            Log.i("TEST", message);
        }
    }

    AsyncPictureProcessing currentTask = null;

    void takePicture() {
        Log.i("TEST", "Taking a picture");
        cameraKitView.captureImage(new CameraKitView.ImageCallback() {
            @Override
            public void onImage(CameraKitView cameraKitView, byte[] bytes) {
                currentTask = new AsyncPictureProcessing(detector, faceDetector);
                currentTask.execute(bytes);
            }
        });
    }

}
