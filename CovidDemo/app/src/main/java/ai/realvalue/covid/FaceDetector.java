package ai.realvalue.covid;

import androidx.appcompat.app.AppCompatActivity;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import android.content.Context;
import android.graphics.Bitmap;;
import android.util.Log;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class FaceDetector {

    private Context context;
    private Interpreter tflite;

    FaceDetector(Context context) {
        this.context = context;
        loadModel("face_mask_detection.tflite");
    }



    TensorImage bitmapToTensor(Bitmap bitmap) {
        // Initialization code
        // Create an ImageProcessor with all ops required. For more ops, please
        // refer to the ImageProcessor Architecture section in this README.
        ImageProcessor imageProcessor = new ImageProcessor.Builder()
                //.add(new ResizeOp(260, 260, ResizeOp.ResizeMethod.BILINEAR))
                .add(new ResizeWithCropOrPadOp(260, 260))
                .build();

        TensorImage tImage = new TensorImage(DataType.FLOAT32);

        // Analysis code for every frame
        tImage.load(bitmap);
        return imageProcessor.process(tImage);
    }

    void process(Bitmap image) {
        TensorBuffer[] probabilities = runInference(image);
        TensorBuffer boxes = probabilities[0];
        TensorBuffer classes = probabilities[1];

        int maxIdx =  -1;
        float maxClass =  0;
        float maxClassProba = 0;

        for(int idx = 0; idx < 5972; idx++) {
            for(int classId = 0; classId <= 1; classId++) {
                float v = classes.getFloatValue(idx * 2 + classId);
                if (v > maxClassProba) {
                    maxClassProba = v;
                    maxClass = classId;
                    maxIdx = idx;
                }
            }
        }

        float[] loc = boxes.getFloatArray();

        float xmin = Math.max(0, (int)(loc[maxIdx*4+0] * image.getWidth()));
        float ymin = Math.max(0, (int)(loc[4*maxIdx+1] * image.getHeight()));
        float xmax = Math.min((int)(loc[4*maxIdx+2] * image.getWidth()), image.getWidth());
        float ymax = Math.min((int)(loc[4*maxIdx+3] * image.getHeight()), image.getHeight());

        Log.i("TEST", "Classe = " + maxClass + " @ " + maxClassProba);
        Log.i("TEST", "" + xmin + ", " + ymin + ", " + xmax + ", " + ymax);
    }

    boolean loadModel(String filepath) {
        // Initialise the model
        try {
            MappedByteBuffer tfliteModel = FileUtil.loadMappedFile(context, filepath);
            tflite = new Interpreter(tfliteModel);
            return true;
        } catch (IOException e) {
            Log.e("TEST", "Failed to load", e);
            return false;
        }
    }

    TensorBuffer[] runInference(Bitmap image) {
        assert(tflite != null);

        TensorImage inputImage = bitmapToTensor(image);
        TensorBuffer[] outputs = {
                TensorBuffer.createFixedSize(new int[]{1, 5972, 4}, DataType.FLOAT32),
                TensorBuffer.createFixedSize(new int[]{1, 5972, 2}, DataType.FLOAT32)
        };
        Map<Integer, Object> outputsMap = new HashMap();
        for(int i = 0; i < outputs.length; i++) { outputsMap.put(i, outputs[i].getBuffer()); }

        tflite.runForMultipleInputsOutputs(new Object[]{inputImage.getBuffer()}, outputsMap);
        return outputs;
    }
}
