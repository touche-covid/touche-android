# Touché Covid Android App

This Android app is [our](#Team) contribution to the Covid-19 crisis. It is part of a larger solution presented at a hackathon/startupweekend - April 2020.

## Installation

Check out the "Downloads" section of this BitBucket repository and fetch the latest APK.  
Deploy it on your Android device. This may require allowing [third party sources which may be a security risk](https://arstechnica.com/gaming/2020/04/google-wins-victory-royale-over-epic-games-snags-fortnite-for-google-play/).

## Usage and Limitations

One the app is installed on your smartphone, it should be rather self explanatory.

Because we are doing video processing and ML inference on the device, a recent and powerful enough device is required for a smooth experience.

## Team

  - Andry
  - Anupam
  - Ialifinaritra
  - Nassim
  - Pierre
  - Voahary

## License

[MIT](https://choosealicense.com/licenses/mit/)


